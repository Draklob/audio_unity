﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CombatMusicControl : MonoBehaviour {

	public AudioMixerSnapshot outOfCombat;
	public AudioMixerSnapshot inCombat;
	public AudioClip[] stings;
	public AudioSource stingSource;
	public float bpm = 128;

	private float m_TransitionIn;
	private float m_TransitionOut;
	private float m_QuaterNote;

	// Use this for initialization
	void Start () {
		m_QuaterNote = 60 / bpm;
		m_TransitionIn = m_QuaterNote;
		m_TransitionOut = m_QuaterNote * 32;
	}

	private void OnTriggerEnter( Collider other )
	{
		if( other.CompareTag("CombatZone"))
		{
			inCombat.TransitionTo(m_TransitionIn);
			PLaySting();
		}
	}

	private void OnTriggerExit( Collider other )
	{
		if( other.CompareTag("CombatZone"))
			outOfCombat.TransitionTo(m_TransitionOut);
	}

	void PLaySting()
	{
		int randClip = Random.Range( 0, stings.Length );
		stingSource.clip = stings[randClip];
		stingSource.Play();
	}
}
